public class Card {
	private String suit;
	private String value;
	private double score;
	
	public Card (String value, String suit, double score) {
		this.suit = suit;
		this.value = value;
		this.score = score;
	}
	public String getSuit() {
		return this.suit;
	}
	public String getValue() {
		return this.value;
	}
	public double getScore() {
		return this.score;
	}
	public String toString() {
		return this.value + " of " + this.suit;
	}
	public double calculateScore() {
		if (this.suit.equals("Heart")) {
			this.score += 0.4;
		} else if (this.suit.equals("Spades")) {
			this.score += 0.3;
		} else if (this.suit.equals("Diamond")) {
			this.score += 0.2;
		} else {
			this.score += 0.1;
		}
		return this.score;
	}
}