public class SimpleWar {
	public static void main(String[] args) {
		
		//Creates a new Deck that will be used to play the game War
		Deck deckCards = new Deck();
		
		//The Deck will be shuffled using the shuffle() method in the Deck object before the game starts
		deckCards.shuffle();
		System.out.println(deckCards);
		
		//Initializes the points of Player 1 & Player 2 to 0
		int player1Points = 0;
		int player2Points = 0;
		
        //Creates a loop to play the game UNTIL no more cards are in the deck	
		while (deckCards.length() != 0) {
		  
		  //Draws card for Player1, PRINTS IT, and CALCULATE ITS SCORE
		  Card card1 = deckCards.drawTopCard();
		  System.out.println(card1);
		  System.out.println(card1.calculateScore());
		  
		  //Draws card for Player2, PRINTS IT, and CALCULATE ITS SCORE
		  Card card2 = deckCards.drawTopCard();
		  System.out.println(card2);
		  System.out.println(card2.calculateScore());
		  
		  //If the score of the first card is bigger than the second card, player 1 wins and vice-versa
		  //Prints the current score of Player 1 AND Player 2
		  if (card1.getScore() > card2.getScore()) {
		    System.out.println("Player 1 Wins!");
		    player1Points++;
		    System.out.println("Player 1 Points: " + player1Points + " \nPlayer 2 Points: " + player2Points);
		  } else {
		    System.out.println("Player 2 Wins!");
		    player2Points++;
		    System.out.println("Player 1 Points: " + player1Points + " \nPlayer 2 Points: " + player2Points);
		  }
		}
		
		//When there is no more cards, if Player 1 and Player 2 have the same points it is a tie, otherwhise there is a winner!
		if (player1Points == player2Points) {
			System.out.println("Congratulations to both players! It was a tie!");
		} else if (player1Points > player2Points) {
			System.out.println("Congratulations Player 1! You won the game!");
		} else {
			System.out.println("Congratulations Player 2! You won the game!");
		}
	}
}
